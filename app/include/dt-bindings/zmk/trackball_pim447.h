/*
 * Copyright (c) 2021 The ZMK Contributors
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#define PIM447_NONE   0
#define PIM447_MOVE   1
#define PIM447_SCROLL 2
#define PIM447_TOGGLE 3

#define PIM447_NORM_EUCLID 1
#define PIM447_NORM_MAX    2
